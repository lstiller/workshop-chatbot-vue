module.exports = {
  devServer: {
    proxy: {
      "^/api/v1/chatbot/": {
        target: "http://localhost:8080",
        changeOrigin: true,
        logLevel: "debug",
        onProxyRes(proxyRes) {
          // Add missing header so XHRs don't complain
          proxyRes.headers["Access-Control-Allow-Origin"] = "*";
        },
      },
    },
  },
};
