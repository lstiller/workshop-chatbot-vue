# chat-box

## Project setup

```
npm install

docker login registry.gitlab.com/lstiller/workshop-chatbot/workshop-chatbot

docker pull registry.gitlab.com/lstiller/workshop-chatbot/workshop-chatbot
```

### Compiles and hot-reloads for development

```
docker run -p 8080:8080 registry.gitlab.com/lstiller/workshop-chatbot/workshop-chatbot

npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
