import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    messages: [
      {
        text: "Hi! I'm here to help you to choose the right light bulb :)",
        author: "server",
      },
      {
        text: "Energy efficiency is important, thats why i recommend you:",
        buttons: ["LED", "CFL", "Difference?"],
        author: "server",
      },
    ],
  },
  mutations: {
    ADD_MESSAGE(state, message) {
      state.messages.push(message);
    },
  },
  actions: {
    pushMessage({ commit }, message) {
      commit("ADD_MESSAGE", message);
    },
  },
  modules: {},
});
